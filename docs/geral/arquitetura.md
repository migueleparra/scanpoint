# **Arquitetura geral da solução**

<p style="text-align: justify;">
Baseado na compreensão do problema e nos requisitos levantados para o produto, este documento apresenta e detalha a arquitetura geral da solução a ser utilizada no projeto, envolvendo as cinco engenharias do campus UnB-FGA: Engenharia Aeroespacial, Engenharia Automotiva, Engenharia de Energia, Engenharia de Software e Engenharia Eletrônica.
</p>

<p style="text-align: justify;"> O detalhamento das arquiteturas específicas podem ser encontrados nos documentos abaixo: </p>

  1. [Software](../software/arquitetura.md);
  2. [Eletrônica](../eletronica-energia/arquitetura_eletronica.md);
  3. [Energia](../eletronica-energia/arquitetura_energia.md);
  4. [Estruturas](../estruturas/arquitetura_subs_estruturas.md);

## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 1.0 | 03/04/2024 | Criação do documento de arquitetura geral | Brenda Santos |
| 1.1 | 03/05/2024 | Adição do documento de arquitetura de todas áreas | Pedro Menezes Rodiguero |
| 1.2 | 03/05/2024 | Adicionando arquitetura de energia e eletronica | Brenda Santos |
| 1.3 | 04/05/2024 | Ajustes finais | Ana |
