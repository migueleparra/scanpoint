# Documento de Identidade

## Visão geral do produto
<p style="text-align: justify;"> O software do ScanPoint é responsável por ser a interface com o usuário  no processo de escaneamento e reprodução 3D de objetos físicos. Além disso, é responsável por receber os pontos coletados, transformá-los em uma malha de pontos e devolver o arquivo stl ao usuário no final do processo. </p>

## Logo e Identidade Visual
<p style="text-align: justify;"> A identidade visual do ScanPoint é representada por sua logo distintiva, que encapsula a essência do produto. O design da logo reflete a ideia do produto, que seria uma solução compacta para escanear e disponibilizar os escaneamentos. </p>

![Logo do PointScan](../assets/identidade/scanpoint-logo.png)
<font size="2"><p style="text-align: center">Fonte: [Logo do PointScan](https://www.canva.com).</p></font>

## Cores
<p style="text-align: justify;"> As cores escolhidas para o ScanPoint foram cuidadosamente selecionadas para harmonizar com a logo e trazer uma experiência contínua e homogênea ao experimentar as interfaces do produto. Para isso, foram selecionadas: </p>

![Cores usadas no PointScan](../assets/identidade/cores.png)
<font size="2"><p style="text-align: center">Fonte: Autoria, 2024.</p></font>

## Tipografia
<p style="text-align: justify;"> A tipografia desempenha um papel fundamental na comunicação visual do ScanPoint. A escolha da fonte Nunito Sans reflete o compromisso com a clareza e a legibilidade, garantindo uma experiência de usuário agradável e intuitiva. </p>

![Tipografia usada no PointScan](../assets/identidade/tipografia.png)
<font size="2"><p style="text-align: center">Fonte: Autores, 2024.</p></font>

<p style="text-align: justify;"> No ScanPoint a fonte utilizada é a Nunito Sans. Para título as fontes são negritas e de tamanho 32, para parágrafo e textos comuns o tamanho é 20 e para subtítulos as fontes são semi negrito de tamanho 26. </p>

## Ícones e Elementos Gráficos
<p style="text-align: justify;"> Os ícones e elementos gráficos utilizados no ScanPoint são projetados para facilitar a compreensão e a navegação do usuário. Cada ícone foi cuidadosamente escolhido para representar uma função específica, garantindo uma experiência de usuário coesa e intuitiva. </p>

<font size="2"><p style="text-align: center">Tabela 1 : Descrição dos ícones</p></font>

| Ícone | Descrição |
| --- | --- |
| ![Ícone para mensagens de sucesso](../assets/identidade/check.png) | Mensagem de sucesso |
| ![Ícone para mensagens de erro ou falha](../assets/identidade/x.png) | Mensagem de erro ou falha |
| ![Ícone para representar o tempo](../assets/identidade/time.png) | Representação do tempo |
| ![Ícone para cancelamento e exclusão](../assets/identidade/xx.png) | Cancelamento e exclusão |
| ![Ícone para re-inicar ou recarregar](../assets/identidade/reload.png) | Reiniciar ou recarregar |
| ![Ícone para próximo passo](../assets/identidade/v.png) | Próximo passo |
| ![Ícone para baixar arquivo](../assets/identidade/baixar.png) | Baixar arquivo |
| ![Ícone para sair](../assets/identidade/sair.png) | Sair |

<font size="2"><p style="text-align: center">Fonte: Autoria, 2024.</p></font>


<font size="2"><p style="text-align: center">Tabela 2 : Descrição dos elementos</p></font>

| Elemento | Descrição |
| --- | --- |
| ![Elemento 1](../assets/identidade/elemento1.png) | Descrição do Elemento 1 |
| ![Elemento 2](../assets/identidade/elemento2.png) | Descrição do Elemento 2 |
| ![Elemento 3](../assets/identidade/elemento3.png) | Descrição do Elemento 3 |
| ![Logo da UnB com FGA](../assets/identidade/unb-fga.png) | Logo da UnB com FGA |

<font size="2"><p style="text-align: center">Fonte: Autoria, 2024.</p></font>

## Layout e Grid:

<p style="text-align: justify;"> Para manter o padrão em todas as telas foi utilizado o um grid com 6 linhas e 11 colunas. </p>

![Grid](../assets/identidade/grid.png)

<font size="2"><p style="text-align: center">Fonte: Autoria, 2024.</p></font>

## Componentes de Interface do Usuário

### Botões

<font size="2"><p style="text-align: center">Tabela 3: Descrição de botões.</p></font>

| Botão | Descrição |
| --- | --- |
| ![Botão para iniciar habilitado](../assets/identidade/botao-iniciar1.png) | Botão para iniciar (habilitado) |
| ![Botão para iniciar desabilitado](../assets/identidade/botao-iniciar2.png) | Botão para iniciar (desabilitado) |
| ![Botão para baixar arquivo](../assets/identidade/botao-baixar.png) | Botão para baixar arquivo |
| ![Botão para cancelar](../assets/identidade/botao-cancelar.png) | Botão para cancelar |
| ![Botão para re-iniciar](../assets/identidade/botao-reload.png) | Botão para reiniciar |
| ![Botão para sair](../assets/identidade/botao-sair.png) | Botão para sair |

<font size="2"><p style="text-align: center">Fonte: Autoria, 2024.</p></font>

### Barras de progresso

![Barra de progresso](../assets/identidade/barra-de-carregamento.png)

<font size="2"><p style="text-align: center">Fonte: Autoria, 2024.</p></font>

## Navegação

<p style="text-align: justify;"> Com o objetivo de melhorar a experiência do usuário na aplicação foi criada uma barra de navegação no topo da tela para facilitar a transição entre as telas sobre e do processo em si, como demonstrado na imagem abaixo: </p>

![Barra de navegação](../assets/identidade/nav-bar.png)

<font size="2"><p style="text-align: center">Fonte: Autoria, 2024.</p></font>

## Responsividade

<p style="text-align: justify;"> A responsividade é uma característica fundamental do ScanPoint, garantindo uma experiência consistente e otimizada em uma variedade de tamanhos de tela. A interface do usuário foi projetada para se adaptar dinamicamente às diferentes resoluções e proporções, garantindo que os usuários possam acessar e utilizar o ScanPoint de forma eficaz, apesar de não poder utilizar em dispositivos diferentes de desktop. </p>

## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 0.1 | 27/04/2024 | Criação da estrutura do documento | Carla R. Cangussú |
| 0.2 | 27/04/2024 | Feito até Ícones e Elementos Gráficos | Carla R. Cangussú |
| 0.3 | 29/04/2024 | Finalização do documento | Carla R. Cangussú |
| 0.4 | 29/04/2024 | Ajuste de formatação | Carla R. Cangussú |
| 0.5 | 29/04/2024 | Ajuste no caminho de algumas imagens | Carla R. Cangussú |
| 1.0 | 29/04/2024 | Arrumando paths, imagens, textos e formatação | Brenda Santos |
| 1.1 | 03/05/2024 | Adicionando tabela e legenda nas imagens | Guilherme Basilio |
| 1.2 | 04/05/2024 | Ajuste de alinhamento, fonte, erros de português | Ana Carolina |