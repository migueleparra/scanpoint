# Estrutura Analítica do Projeto - EAP

## 1. Introdução

<p style="text-align: justify;"> A Estrutura Analítica do Projeto (EAP) é uma ferramenta fundamental no gerenciamento de projetos, que tem a finalidade de organizar e decompor as entregas e tarefas de um projeto em componentes menores e mais gerenciáveis. Representada em forma de diagrama hierárquico, a EAP é apresentada em níveis, começando com as entregas principais do projeto e dividindo-as em sub-entregas e tarefas cada vez menores. </p>

<p style="text-align: justify;"> Essa abordagem possibilita uma visão geral do projeto, permitindo a identificação dos principais entregáveis e das atividades necessárias para a conclusão bem-sucedida do projeto, independentemente do seu tamanho ou complexidade. Além disso, a EAP desempenha um papel crucial no escopo do projeto, na definição de responsabilidades e na estipulação de marcos e entregas essenciais. </p>

## 2. Metodologia

<p style="text-align: justify;"> Esse documento foi elaborado seguindo as diretrizes do plano de ensino da matéria. Com isso, foram definidas as hierarquias de mais alto nível em cada etapa do projeto, orientando nas entregas de cada fase. </p>

## 3. Estrutura

<p style="text-align: justify;"> A EAP é uma árvore que possui três níveis: </p>

* **Nível 1**: O nível 1 da EAP é composto pelas atividades de alto nível do projeto. Essas atividades são chamadas de atividades de projeto. Elas são as atividades que representam o projeto como um todo.

* **Nível 2**: O nível 2 da EAP é composto pelas atividades de nível intermediário do projeto. Essas atividades são chamadas de atividades de pacote. Elas são as atividades que representam um conjunto de atividades de nível mais baixo.

* **Nível 3**: O nível 3 da EAP é composto pelas atividades de baixo nível do projeto. Essas atividades são chamadas de atividades de trabalho. Elas são as atividades que representam as tarefas mais básicas do projeto.

## 4. Planilha EAP

<iframe width="768" height="768" src="https://docs.google.com/spreadsheets/d/1KWgKsgf_MtKPNWU6sujn9LvrJqU3NcNO0QaLYJdHL4Q/edit?usp=sharing" frameborder="0" scrolling="no" allow="fullscreen; clipboard-read; clipboard-write" allowfullscreen></iframe>

<font size="2"><p style="text-align: center">Fonte: Tabela de EAP - Autoria Própria.</p></font>

## 5. Resultado

<iframe width="768" height="432" src="https://miro.com/app/live-embed/uXjVKVIp7dQ=/?share_link_id=749223997538&moveToViewport=-1364,579,3030,1328&embedId=707518803643" frameborder="0" scrolling="no" allow="fullscreen; clipboard-read; clipboard-write" allowfullscreen></iframe>

<font size="2"><p style="text-align: center">Fonte: Autoria Própria.</p></font>

## 6. Referências

> EUAX. EAP (Estrutura Analítica do Projeto): o que é, como fazer e qual a diferença entre EAP e Cronograma. Disponível em: https://www.euax.com.br/2018/12/eap-estrutura-analitica-projeto/. Acesso em: 12 de Abril de 2024.

> WBS - IBM. Disponível em https://www.ibm.com/docs/en/mea/761?topic=SSXB7Z_7.6.1/com.ibm.meaora.doc/sag/c_ctr_oraclepa_proj_task_integ.html. Acesso em: 12 de Abril de 2024.

> PMI. PMBOK® Guide. Disponível em: https://www.pmi.org/pmbok-guide-standards/foundational/pmbok. Acesso em: 12 de Abril de 2024.

## Versionamento

| Versão | Data       | Modificação                           | Autor                                    |
| ------ | ---------- | ------------------------------------- | ---------------------------------------- |
| 0.1    | 12/04/2024 | Criação do documento                  | Ana Carolina Rodrigues e Denniel William |
| 0.2    | 12/04/2024 | Introdução e Metodologia              | Ana Carolina Rodrigues e Denniel William |
| 0.3    | 12/04/2024 | Adição do link do eap                 | Ana Carolina Rodrigues e Denniel William |
| 1.0    | 29/04/2024 | Adição da planilha do EAP             | Brenda Santos e Denniel William          |
| 1.1    | 03/05/2024 | Alteração da posição do versionamento | Ciro Costa                               |
| 1.2    | 04/05/2024 | Ajustes de alinhamento e formatação   | Ana                                      |
