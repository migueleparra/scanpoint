# Desenho Técnicos

<p style="text-align: justify;">
Os desenhos técnicos estão apresentados nesta seção a partir de subsistemas e componentes.
</p>

**Observações:**

 - Para visualização dos arquivos de draftings basta acessar o repositório em [repositório de estruturas - draftings](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-08/scanpoint/-/tree/main/docs/estruturas/Draftings), assim como também para todos os CADs, que podem ser acessados no [repositório de estruturas - CAD](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-08/scanpoint/-/tree/main/docs/estruturas/CAD/Estrutura_CATIA?ref_type=heads).

- Os draftings também podem ser facilmente visualizados através do [download deste documento](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-08/scanpoint/-/raw/main/docs/estruturas/Draftings/00-Montagem_merged.pdf?inline=false).

## Sistema Montado
![Montagem](../estruturas/imagens/DTs/Montagem-1.png)

## Sistema Explodido
![Vista Explodida](../estruturas/imagens/DTs/Vista_explodida-1.png)

## Subsistema Estrutural

![Subsistema estrutural](../estruturas/imagens/DTs/Subsistemas/Subsistema_estrutural-1.png)

## Subsistema elevação do sensor 

![subsistema elevação](../estruturas/imagens/DTs/Subsistemas/Subsistema_de_elevacao_da_camera-1.png)

## Subsistema Prato Giratório

![Subsistema Prato Giratório](../estruturas/imagens/DTs/Subsistemas/Subsistema_do_prato_giratorio-1.png)

## DTs Componentes 

![1](../estruturas/imagens/DTs/Part/Caixa_eletronica_tampa-1.png)

![2](../estruturas/imagens/DTs/Part/estrutura_aluminio_estrudado-1.png)

![3](../estruturas/imagens/DTs/Part/estrutura_cantoneira_20x20-1.png)

![4](../estruturas/imagens/DTs/Part/Prato_acoplamento-1.png)

![5](../estruturas/imagens/DTs/Part/prato_suporte-1.png)

![6](../estruturas/imagens/DTs/Part/prato_suporte_motor-1.png)

![7](../estruturas/imagens/DTs/Part/trilho_castanha-1.png)

![8](../estruturas/imagens/DTs/Part/trilho_encaixe_camera-1.png)

![9](../estruturas/imagens/DTs/Part/trilho_encaixe_trilho-1.png)

![10](../estruturas/imagens/DTs/Part/trilho_encaixe_trilho_inferior-1.png)


