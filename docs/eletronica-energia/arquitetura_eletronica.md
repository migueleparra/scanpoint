# Arquitetura do Subsistema de Eletrônica

<p style="text-align:justify;">
O principal objetivo da execução do projeto do subsistema eletrônico é de garantir o controle preciso e eficiente do sistema, permitindo o funcionamento adequado do scanner e da mesa rotatória. Para isso, é necessário selecionar e integrar os componentes eletrônicos de forma a atender aos requisitos funcionais e não funcionais estabelecidos.
</p>

<p style="text-align:justify;">
O motor de passo será detalhado em sua estrutura interna, destacando cada componente relevante para o funcionamento do sistema. Além disso, serão realizadas análises matemáticas para compreender o comportamento do motor em relação ao tempo, utilizando as especificações técnicas fornecidas pelo fabricante.
</p>

<p style="text-align:justify;">
Os microcontroladores Arduino Uno são responsáveis pela manipulação dos dados de escaneamento e pelo controle dos motores de passo. Serão abordadas todas as características pertinentes destes microcontroladores, destacando sua importância para a integração e funcionamento do sistema.
</p>

<p style="text-align:justify;">
A ponte H, representada pelo componente L298N, facilita a comunicação dos motores com o Arduino, possibilitando o controle eficiente conforme proposto no projeto.
</p>

<p style="text-align:justify;">
Considerando os requisitos de alimentação do sistema, será adotada uma bateria de 12V para os motores de passo, juntamente com um dispositivo regulador, como o LM2596, para fornecer a tensão necessária para alimentar a parte eletrônica do sistema, que requer 5V.
</p>

<p style="text-align:justify;">
Ao longo do relatório, serão apresentadas imagens de simulação dos circuitos, utilizando softwares como Proteus e Fritzing, juntamente com a geração de diagramas de blocos do circuito e diagramas de barramento para a parte dos motores, proporcionando uma visão abrangente e detalhada da arquitetura do subsistema eletrônico do protótipo de mesa scanner.
</p>

## Versionamento

| Versão | Data       | Modificação             | Autor       |
| ------ | ---------- | ----------------------- | ----------- |
| 0.1    | 03/05/2024 | Criação do documento    | Carolina Oliveira, Miguel Munoz |
| 0.2    | 03/05/2024 | Adição de versionamento | Ciro Costa  |
| 0.3    | 04/05/2024 | Ajustes de formatação | Ana Carolina |
| 0.4    | 04/05/2024 | Atualização da formatação | Carolina |
